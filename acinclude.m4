dnl
dnl Add macros
dnl	JAPHAR_GREP_CFLAGS
dnl

dnl
dnl JAPHAR_GREP_CFLAGS(flag, cmd_if_missing, cmd_if_present)
dnl
dnl From Japhar.  Report changes to japhar@hungry.com
dnl
AC_DEFUN(JAPHAR_GREP_CFLAGS,
[case "$CFLAGS" in
"$1" | "$1 "* | *" $1" | *" $1 "* )
  ifelse($#, 3, [$3], [:])
  ;;
*)
  $2
  ;;
esac
])

AC_DEFUN(PERE_DETECT_BROKEN_SETNETGRENT,
[
  AC_CACHE_CHECK([for broken setnetgrent], pere_cv_broken_setnetgrent,
  [
    AC_TRY_COMPILE(
      [#include <netdb.h>],
      [int retval = setnetgrent("dummygroup")],
      pere_cv_broken_setnetgrent=no, pere_cv_broken_setnetgrent=yes)
  ])
  if test "$pere_cv_broken_setnetgrent" = yes ; then
    AC_DEFINE_UNQUOTED(HAVE_BROKEN_SETNETGRENT, 1,
	[Define if setnetgrent() have no return value.])
  fi
])
