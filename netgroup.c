/*
 * Author: Petter Reinholdtsen <pere@hungry.com>
 * Date:   2002-11-16
 *
 * License: GNU Public License
 *
 * This program was inspired from the NetBSD program with the same name.
 */

#include "config.h"

#include <stdio.h>
#include <errno.h>
#define __USE_MISC /* Make the netgroup prototypes visible on Linux [2006-06-11] */
#include <netdb.h>

#ifdef HAVE_GETOPT_H
#  include <getopt.h>
#else /* not HAVE_GETOPT_H */
extern int optind;
#endif

#ifdef HAVE_BROKEN_SETNETGRENT
/* The broken setnetgrent() on Irix and AIX do not have a return value */
int
SETNETGRENT(const char *netgroup)
{
  setnetgrent(netgroup);
  return 1;
}
#define setnetgrent SETNETGRENT
#endif

#ifdef HAVE_GETOPT_LONG
#  define GETOPT_LONG getopt_long

static struct option long_options[] =
{
  {"domain",  0, 0, 'd'},
  {"host",    0, 0, 'h'},
  {"user",    0, 0, 'u'},
  {"version", 0, 0, 'V'},
  {0, 0, 0, 0}
};

#else /* not HAVE_GETOPT_LONG */
#  define GETOPT_LONG(argc,argv,optstr,lopts,lidx) getopt(argc,argv,optstr)
#endif /* not HAVE_GETOPT_LONG */

static void
feature_report(void)
{
#ifndef HAVE_GETOPT_LONG
  printf("\n *** The long options are not available on this platform");
#endif /* not HAVE_GETOPT_LONG */
#ifdef HAVE_BROKEN_SETNETGRENT
  printf("\n"
         " *** It is impossible differenciate between empty and missing\n"
         "     netgroups on this platform because of a broken setnetgrent()\n"
         "     implementation.");
#endif /* HAVE_BROKEN_SETNETGRENT */
  printf("\n");
}

static void
usage(const char *progname)
{
  printf("Usage: %s [-dhuV] netgroup\n", progname);
  feature_report();
}

static void
version(void)
{
  printf("netgroup (" PACKAGE ") " VERSION);

  feature_report();
}

int main(int argc, char *argv[])
{
  char *member[3] = {0,0,0};
  int memberindex = 0;
  const char *netgroup = NULL;
  int opt;
#ifdef HAVE_GETOPT_LONG
  int option_index;
#endif /* HAVE_GETOPT_LONG */

  do {
    opt = GETOPT_LONG(argc, argv, "dhuV", long_options, &option_index);
    switch (opt)
      {
      case 'h':
        memberindex = 0;
        break;
      case 'u':
        memberindex = 1;
        break;
      case 'd':
        memberindex = 2;
        break;
      case 'V':
        version();
        return 0;
        break;
      case -1:
        break;
      default:
        usage(argv[0]);
        return 1;
      }
  } while (-1 != opt);

  if (optind < argc)
    netgroup = argv[optind++];
  else
    {
      printf("Missing netgroup\n");
      usage(argv[0]);
      return 1;
    }

  if ( ! setnetgrent(netgroup) )
    {
      perror("error: setnetgrent() failed");
      return 1;
    }

  while ( getnetgrent(&member[0], &member[1], &member[2]) )
    {
      if (member[memberindex])
        printf("%s\n", member[memberindex]);
    }

  endnetgrent();

  return 0;
}
