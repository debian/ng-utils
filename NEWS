New in 1.1 released 2023-02-13
 * New project home on
   <URL: https://salsa.debian.org/debian/ng-utils >.

New in 1.0 released 2014-02-22
 * New project home on
   <URL: https://alioth.debian.org/projects/ng-utils/ >.
 * Migrated to git.  Drop obsolete CVS related stuff.

New in 0.8 released 2013-06-28:
 * Fix segfault with unknown command line arguments.

New in 0.7 released 2008-04-13:
 * Add -Wformat as build flag.

New in 0.6 released 2005-04-07:
 * Include HACKING and excludegroup.diff in tarball.
 * Use AM_MAINTAINER_MODE in configure.ac.

New in 0.5 released 2004-12-19:
 * Updated documentation.
 * Minor bugfixes.

New in 0.4 released 2003-02-13:
 * Moved debian package rules from deb/ to debian/.
 * Updated the README.
 * Add info about broken API in status output at the end of configure.

New in 0.3 released 2002-11-30:
 * Fix typos in code to handle broken setnetgrent().
 * Add debian package rules.

New in 0.2 released 2002-11-29:
 * Changed behaviour and output to match NetBSD.
 * Added code to detect the broken setnetgrent() on Irix, and some code
   to work around this problem.

New in 0.1 released 2002-11-17:
 * First release.
 * Implemented innetgr and netgroup based on manpages from NetBSD.
