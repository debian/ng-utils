/*
 * Author: Petter Reinholdtsen <pere@hungry.com>
 * Date:   2002-11-16
 *
 * License: GNU Public License
 *
 * This program was inspired from the NetBSD program with the same name.
 */

#include "config.h"

#define __USE_MISC /* Find innetgr() in GNU libc <netdb.h> */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#define __USE_MISC /* Make the netgroup prototypes visible on Linux [2006-06-11] */
#include <netdb.h>

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else /* not HAVE_GETOPT_H */
extern char *optarg;
extern int optind;
#endif /* not HAVE_GETOPT_H */

#ifdef HAVE_GETOPT_LONG
#  define GETOPT_LONG getopt_long

static struct option long_options[] =
{
  {"domain",  1, 0, 'd'},
  {"host",    1, 0, 'h'},
  {"user",    1, 0, 'u'},
  {"verbose", 0, 0, 'v'},
  {"version", 0, 0, 'V'},
  {0, 0, 0, 0}
};

#else /* not HAVE_GETOPT_LONG */
#  define GETOPT_LONG(argc,argv,optstr,lopts,lidx) getopt(argc,argv,optstr)
#endif /* not HAVE_GETOPT_LONG */

static void
feature_report(void)
{
#ifndef HAVE_GETOPT_LONG
  printf("\n *** The long options are not available on this platform");
#endif /* not HAVE_GETOPT_LONG */
  printf("\n");
}

static void
usage(const char *progname)
{
  printf("Usage: %s [-d domain] [-h host] [-u user] [-vV] netgroup\n",
         progname);
  feature_report();
}

static void
version(void)
{
  printf("innetgr (" PACKAGE ") " VERSION);
  feature_report();
}

int main(int argc, char *argv[])
{
  int verbose = 0;
  char *machine = NULL, *user = NULL, *domain = NULL;
  const char *netgroup = NULL;
  int retval = 0;
  int opt;
#ifdef HAVE_GETOPT_LONG
  int option_index;
#endif /* HAVE_GETOPT_LONG */

  do {
    opt = GETOPT_LONG(argc, argv, "d:h:u:vV", long_options, &option_index);
    switch (opt)
      {
      case 'd':
        domain = optarg;
        break;
      case 'h':
        machine = optarg;
        break;
      case 'u':
        user = optarg;
        break;
      case 'v':
        verbose = 1;
        break;
      case -1:
        break;
      case 'V':
        version();
        return 0;
        break;
      default:
        usage(argv[0]);
        return 2;
      }
  } while (-1 != opt);

  if (optind < argc)
    netgroup = argv[optind++];
  else
    {
      printf("Missing netgroup\n");
      usage(argv[0]);
      return 2;
    }

  if ( ! (domain || machine || user))
    {
      printf("You need to specify one of domain, machine or user.\n");
      usage(argv[0]);
      return 2;
    }

  retval = innetgr(netgroup, machine, user, domain);

  if (verbose)
    printf("%s: %d\n", netgroup, retval);

  if ( ! retval )
    return 1;

  return 0;
}
